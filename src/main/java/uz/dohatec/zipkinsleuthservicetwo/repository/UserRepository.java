package uz.dohatec.zipkinsleuthservicetwo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import uz.dohatec.zipkinsleuthservicetwo.entity.User;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

}

