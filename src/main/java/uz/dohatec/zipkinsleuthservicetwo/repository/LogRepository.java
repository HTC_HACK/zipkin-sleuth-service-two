package uz.dohatec.zipkinsleuthservicetwo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import uz.dohatec.zipkinsleuthservicetwo.entity.Log;

public interface LogRepository extends MongoRepository<Log,String> {
}
