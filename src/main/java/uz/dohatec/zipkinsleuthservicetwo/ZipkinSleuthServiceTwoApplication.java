package uz.dohatec.zipkinsleuthservicetwo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZipkinSleuthServiceTwoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZipkinSleuthServiceTwoApplication.class, args);
    }

}
