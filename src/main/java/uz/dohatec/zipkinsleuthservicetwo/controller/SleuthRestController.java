package uz.dohatec.zipkinsleuthservicetwo.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import uz.dohatec.zipkinsleuthservicetwo.service.LogService;

@RestController
@RequestMapping("/api/v1/service_two")
@Slf4j
public class SleuthRestController {

    private final LogService logService;
    private final RestTemplate restTemplate;

    @Autowired
    public SleuthRestController(LogService logService, RestTemplate restTemplate) {
        this.logService = logService;
        this.restTemplate = restTemplate;
    }


    @GetMapping
    public String serviceTwo() {
        String data = "service two";
        logService.save();
        return data;
    }

    @GetMapping("/call")
    public String callServiceOne() {
        String data = "service one";
        logService.save();
        String res = restTemplate.getForObject("http://localhost:8080/api/v1/service_one", String.class);
        return data + " | " + res;
    }

}
