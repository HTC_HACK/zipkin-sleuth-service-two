package uz.dohatec.zipkinsleuthservicetwo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.Span;
import org.springframework.cloud.sleuth.Tracer;
import org.springframework.stereotype.Service;
import uz.dohatec.zipkinsleuthservicetwo.entity.Log;
import uz.dohatec.zipkinsleuthservicetwo.repository.LogRepository;

@Service
@Slf4j
public class LogService {

    private final LogRepository logRepository;

    private final Tracer tracer;

    @Autowired
    public LogService(LogRepository logRepository, Tracer tracer) {
        this.logRepository = logRepository;
        this.tracer = tracer;
    }


    public void save() {
        Span span = tracer.currentSpan();
        Log newLog = Log.builder().spanId(span.context().spanId()).traceId(span.context().traceId()).build();
        logRepository.save(newLog);
    }
}
