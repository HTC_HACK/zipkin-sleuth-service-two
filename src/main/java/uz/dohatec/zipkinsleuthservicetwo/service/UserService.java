package uz.dohatec.zipkinsleuthservicetwo.service;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.dohatec.zipkinsleuthservicetwo.entity.User;
import uz.dohatec.zipkinsleuthservicetwo.repository.UserRepository;

@Service
@Slf4j
public class UserService {

    private final UserRepository userRepository;


    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void save() {

        User user = User.builder()
                .username("username")
                .password("password")
                .build();

        userRepository.save(user);
    }
}
