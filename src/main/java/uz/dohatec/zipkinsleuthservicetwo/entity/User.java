package uz.dohatec.zipkinsleuthservicetwo.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@Document("users")
public class User {

    @Id
    private String id;
    private String username;
    private String password;

}
